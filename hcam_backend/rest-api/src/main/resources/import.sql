USE `swtpj`;
SET foreign_key_checks = 0;

Insert Into Position
(building, room, level)
Values
('18', '0', '1');
SET @position1 = LAST_INSERT_ID();

Insert Into Position
(building, room, level)
Values
('42', '5', '69');
SET @position2 = LAST_INSERT_ID();

Insert Into TaskTemplate
(description, `repeatable`, taskType, assetType_id)
Values
('Betten neu beziehen', 1, 'Transport', 1);
SET @tasktemplate1 = LAST_INSERT_ID();

Insert Into TaskTemplate
(description, `repeatable`, taskType, assetType_id)
Values
('Pumpenanlage pruefen', 1, 'Pruefauftrag', 2);
SET @tasktemplate2 = LAST_INSERT_ID();

Insert Into Checkpoints
(`name`, `value`, `description`, `type`, defaultValue, assetType_id, taskTemplate_id, task_id)
Values
('Schanier geoelt', 1, 'WD50 verwenden', 'boolean', 'false', '1', '1', '1');
SET @checkpoint1 =  LAST_INSERT_ID();

Insert Into Checkpoints
(`name`, `value`, `description`, `type`, defaultValue, assetType_id, taskTemplate_id, task_id)
Values
('Kopfkissen neu bezogen', 1, 'Allergiker beachten', 'boolean', 'false', '1', '1', '1');
SET @checkpoint2 =  LAST_INSERT_ID();

Insert Into AssetType
(name, description)
Values
('Modell E034', 'Bitte erst Geraet ausschalten');
SET @assettype1 = LAST_INSERT_ID();

Insert Into AssetType
(name, description)
Values
('Modell XF90', 'Hoehenverstellbar');
SET @assettype2 = LAST_INSERT_ID();

Insert Into Tag
(uuid, category, description, active, asset_id)
Values
('11111', 'Beacon', 'IBeacon', 1, 1);
SET @tag1 = LAST_INSERT_ID();

Insert Into Tag
(uuid, category, description, active, asset_id)
Values
('22222', 'QR', 'QR Code', 0, 2);
SET @tag2 = LAST_INSERT_ID();

Insert Into Tag
(uuid, category, description, active, asset_id)
Values
('33333', 'BARCODE', 'BARCODE', 0, 1);
SET @tag3 = LAST_INSERT_ID();

Insert Into Tag
(uuid, category, description, active, asset_id)
Values
('44444', 'BARCODE', 'BARCODE', 0, 2);
SET @tag4 = LAST_INSERT_ID();

Insert Into Tag
(uuid, category, description, active, asset_id)
Values
('55555', 'BARCODE', 'BARCODE', 0, NULL );
SET @tag5 = LAST_INSERT_ID();

Insert Into Tag
(uuid, category, description, active, asset_id)
Values
('66666', 'BARCODE', 'BARCODE', 0, NULL );
SET @tag6 = LAST_INSERT_ID();


Insert Into Asset
(name, inspected, operative, position_id, assetType_id)
Values
('Brandschutztuer', 0, 1, '1', '1');
SET @asset1 = LAST_INSERT_ID();

Insert Into Asset
(name, inspected, operative, position_id, assetType_id)
Values
('Krankenbett', 1, 0, '1', '1');
SET @asset2 = LAST_INSERT_ID();

Insert Into `Role`
(`name`)
Values
('ADMIN');
SET @role1 = LAST_INSERT_ID();

Insert Into `Role`
(`name`)
Values
('MANAGER');
SET @role2 = LAST_INSERT_ID();

Insert Into `Role`
(`name`)
Values
('LPU');
SET @role3 = LAST_INSERT_ID();

Insert Into User
(forename, surname, department, `password`, username, phone, email, role_id)
Values
('Hans', 'Maelzer','EDV-Leiter','password','yvasylenko','0163254138', 'hans.maelzer@uksh.de', '1');
SET @user1 = LAST_INSERT_ID();

Insert Into User
(forename, surname, department, `password`, username, phone, email, role_id)
Values
('Joachim', 'Bauer','System Admin','password','pdrath','01735876169', 'joachim.bauer@uksh.de', '3');
SET @user2 = LAST_INSERT_ID();

Insert Into User
(forename, surname, department, `password`, username, phone, email, role_id)
Values
('Joachim', 'Bauer','System Admin','password','mkaapke','01735876169', 'joachim.bauer@uksh.de', '2');
SET @user3 = LAST_INSERT_ID();

Insert Into Task
(title, startDate, finishDate, state, taskTemplate_id, position_id, user_id, asset_id)
Values
('Pruefautrag', 20190423055423, 20190601070523, 'NotAssigned', @tasktemplate1, @position1, @user1, @asset1);
SET @task1 = LAST_INSERT_ID();

Insert Into Task
(title, startDate, finishDate, state, taskTemplate_id, position_id, user_id, asset_id)
Values
('Pruefautrag', 20190611110517, 20200115010523, 'Finished', @tasktemplate2, @position2, @user2, @asset2);
SET @task2 = LAST_INSERT_ID();

Insert Into MultimediaFile
(token, `type`, `name`, size, taskTemplate_id, task_id)
Values
('http://www.google.de', 'jpeg', 'Schaltplan', 42, @tasktemplate1, @task1);
SET @file1 =  LAST_INSERT_ID();

Insert Into MultimediaFile
(token, `type`, `name`, size, taskTemplate_id, task_id)
Values
('http://www.google.de', 'pdf', 'Benutzerhandbuch Bettenmodell XF90', 69, @tasktemplate2, @task2);
SET @file2 =  LAST_INSERT_ID();


Insert Into TaskLogEntry
(`timestamp`, sourceTaskState, targetTaskState, task_id)
Values
(20190101050523, NULL, 'NotAssigned', @task1),
(20190401073712, 'WorkInProgress', 'Finished', @task2);


