package backend.repositories;

import backend.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer>{
    <T> Collection<T> findAllProjectedBy(Class<T> projectionClass);

    <T> T findById(int id, Class<T> projection);

    Tag findById(int id);
    
    <T> Collection<T> findByActive(boolean active, Class<T> projection);

    <T> Collection<T> findByCategory(String category, Class<T> projection);

    @Query(value = "SELECT LAST_INSERT_ID()", nativeQuery = true)
    int getLastInsertId();

}
