package backend.repositories;

import backend.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    <T> Collection<T> findAllProjectedBy(Class<T> projectionClass);
    Optional<Role> findRoleById(int id);
}
