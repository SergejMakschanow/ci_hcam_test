package backend.repositories;

import backend.model.TaskTemplate;
import backend.model.projections.TaskTemplateProjection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public interface TaskTemplateRepository extends JpaRepository<TaskTemplate, Integer> {
    <T> Collection<T> findAllProjectedBy(Class<T> projectionClass);
    TaskTemplateProjection findById(Class<TaskTemplateProjection> projectionClass, int id);
    Optional<TaskTemplate> findById(int id);
}
