package backend.repositories;

import backend.model.Asset;
import backend.model.projections.CustomAssetShortList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Optional;

public interface AssetRepository extends JpaRepository<Asset, Integer> {
    <T> Collection<T> findAllProjectedBy(Class<T> projectionClass);
    CustomAssetShortList findById(Class<CustomAssetShortList> projectionClass, int id);
    Optional<Asset> findById(int id);

    @Query(value = "SELECT LAST_INSERT_ID()", nativeQuery = true)
    int getLastInsertId();
}
