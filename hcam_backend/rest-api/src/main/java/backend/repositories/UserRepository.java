package backend.repositories;

import backend.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    <T> Collection<T> findAllProjectedBy(Class<T> projectionClass);
    List<User> findByForename(String forename, Pageable pageable);
    List<User> findBySurname(String surname, Pageable pageable);
    List<User> findByDepartment(String department, Pageable pageable);
    Optional<User> findByUsername(String username);
    User findByPhone(String phone);
    User findByEmail(String email);
}
