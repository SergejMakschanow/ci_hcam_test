package backend.repositories;

import backend.model.Task;
import backend.model.projections.CustomGetFilteredTasks;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    <T> Collection<T> findAllProjectedBy(Class<T> projectionClass);
    Task findById(int id);
    Task findByTitle(String title);
    Task findByState(String state);
    CustomGetFilteredTasks findTaskByUserUsername(String username, Class<CustomGetFilteredTasks> projectionClass);
}
