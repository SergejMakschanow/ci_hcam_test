package backend.controller;

import backend.model.projections.RoleBaseProjection;
import backend.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

@Controller
public class RoleController {

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping(path = "/roles")
    public ResponseEntity getRoles() {
        final Collection<RoleBaseProjection> roles = this.roleRepository.findAllProjectedBy(RoleBaseProjection.class);
        return ResponseEntity.ok(roles);
    }
}
