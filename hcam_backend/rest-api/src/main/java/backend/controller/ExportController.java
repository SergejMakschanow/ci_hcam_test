package backend.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import backend.out.xlsx.TaskExportConfig;
import backend.out.xlsx.TaskExporter;

@Controller
@CrossOrigin
@RequestMapping(ExportController.BASE_URL)
public class ExportController {
	public static final String BASE_URL = "/exports";

	@Autowired
	private TaskExporter taskExporter;

	@GetMapping(path = "/tasks")
	public void download(String closedTasks, String excludeNotAssigned, HttpServletResponse response) {
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.addHeader("Content-Disposition", "attachment; filename=Tasks.xlsx");
		try {
			TaskExportConfig taskConfig = new TaskExportConfig();
			taskConfig.closedTasks = Boolean.valueOf(closedTasks);
			taskConfig.excludeNotAssigned = Boolean.valueOf(excludeNotAssigned);
			taskExporter.write(response.getOutputStream(), taskConfig);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
