package backend.controller;

import backend.model.Asset;
import backend.model.Position;
import backend.model.Tag;
import backend.model.projections.CustomAssetShortList;
import backend.model.wrapper.AssetRequestBody;
import backend.repositories.AssetRepository;
import backend.repositories.PositionRepository;
import backend.repositories.TagRepository;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.*;

@Controller
@RequestMapping(AssetController.BASE_URL)
public class AssetController {

    public static final String BASE_URL = "/assets";

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private TagRepository tagRepository;

    @GetMapping
    public ResponseEntity getAllAssets() {

        List<Asset> assets = assetRepository.findAll();

        List<Map> ret = new ArrayList<>();
        for (Asset a : assets) {
            Map<String, Object> entry = new TreeMap<>();
            entry.put("id", a.getId());
            entry.put("name", a.getName());
            entry.put("inspected", a.getInspected());
            entry.put("operative", a.getOperative());
            List<Map> tags = new ArrayList<>();
            for (Tag t : a.getTags()) {
                Map<String, Object> tagNode = new TreeMap<>();
                tagNode.put("uuid", t.getUuid());
                tagNode.put("category", t.getCategory());
                tagNode.put("active", t.getActive());
                tags.add(tagNode);
            }
            entry.put("tags", tags);
            ret.add(entry);
        }
            return ResponseEntity.ok(ret);
    }

    @GetMapping(path = "/shortList/{idTaskTemplate}")
    public ResponseEntity getAllAssetsByIdTaskTemplate(@PathVariable int idTaskTemplate) {
        final CustomAssetShortList assets = this.assetRepository.findById(CustomAssetShortList.class, idTaskTemplate);
        return ResponseEntity.ok(assets);
    }

    @PostMapping
    public ResponseEntity createAsset(@RequestBody @NotNull AssetRequestBody asset) {
        Map<String, Object> error = new HashMap<>();
        String errorMsg = "";
        Asset newEntry = new Asset();
        newEntry.setInspected(asset.inspected);
        newEntry.setOperative(asset.operative);

        Optional<Position> position = positionRepository.findOneByRoomAndBuildingAndLevel(asset.room, asset.building, asset.level);
        if (position.isPresent()) {
            newEntry.setPosition(position.get());
        } else {
            newEntry.setPosition(new Position(asset.building, asset.room, asset.level));
        }

        newEntry.setName(asset.name);
        List<Integer> allreadyMaped = new ArrayList<>();
        for (int id : asset.tags) {
            if (!tagRepository.exists(id)) {
                errorMsg += "some of IDs are not exist";
                error.put("error", errorMsg);
                return ResponseEntity.badRequest().body(error);
            } else {
                Asset a = tagRepository.findById(id).getAsset();
                System.out.println(a);
                if (a != null) {
                    allreadyMaped.add(id);
                }
            }
        }
        if (allreadyMaped.isEmpty()) {
            asset.tags.forEach(id -> newEntry.getTags().add(tagRepository.findById(id)));
            asset.tags.forEach(id -> tagRepository.findById(id).setAsset(newEntry));
            assetRepository.save(newEntry);
            return ResponseEntity.ok().build();
        } else {
            errorMsg += "some of IDs are already assigned to another assets";
            error.put("error", errorMsg);
            return ResponseEntity.badRequest().body(error);
        }
    }
}
