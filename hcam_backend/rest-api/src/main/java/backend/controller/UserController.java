package backend.controller;

import backend.model.Role;
import backend.model.User;
import backend.model.projections.CustomDepartmentOnly;
import backend.model.projections.CustomUser;
import backend.model.projections.CustomUserShortList;
import backend.model.wrapper.UserWrapper;
import backend.repositories.RoleRepository;
import backend.repositories.TaskRepository;
import backend.repositories.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-05-01T12:06:10.099Z[GMT]")


@Controller
@RequestMapping(UserController.BASE_URL)
public class UserController  {

    public static final String BASE_URL = "/users";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TaskRepository taskRepository;

    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
    @GetMapping
    public ResponseEntity getAllUsers() {
        final Collection<CustomUser> users = this.userRepository.findAllProjectedBy(CustomUser.class);
        //System.out.println(userRepository.getOne(1).getRole().getUsers());
        return ResponseEntity.ok(users);
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
    @DeleteMapping(path="/{username}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity deleteUser(@PathVariable String username) {
        if (!userRepository.findByUsername(username).isPresent()) {
            System.out.println("User not found");
        }
        int id = userRepository.findByUsername(username).get().getId();
        userRepository.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createUser(@RequestBody UserWrapper user) {
        User newEntry = new User();
        newEntry.setForename(user.forename);
        newEntry.setUsername(user.username);
        newEntry.setSurname(user.surname);
        newEntry.setEmail(user.email);
        newEntry.setPhone(user.phone);
        newEntry.setDepartment(user.department);
        newEntry.setPassword(user.password);

        Optional<Role> role = roleRepository.findRoleById(user.idRole);
        if(role.isPresent()) {
            newEntry.setRole(role.get());
        }

        user.tasks.forEach(id -> newEntry.addToTasks(taskRepository.findById(id)));
        userRepository.save(newEntry);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path="/{username}")
    public User updateUser(@RequestBody User user, @PathVariable String username) {
        User existing = userRepository.findByUsername(username).get();
        copyNonNullProperties(user, existing);
        return userRepository.save(existing);
    }

    @GetMapping(path = "/departments")
    public ResponseEntity findAllDepartments() {
        final Collection<CustomDepartmentOnly> departments = this.userRepository.findAllProjectedBy(CustomDepartmentOnly.class);
        return ResponseEntity.ok(departments);
    }

    @GetMapping(path = "/shortList")
    public ResponseEntity getAllUsersShort() {
        final Collection<CustomUserShortList> shortList = this.userRepository.findAllProjectedBy(CustomUserShortList.class);
        return ResponseEntity.ok(shortList);
    }


    @GetMapping(path="/forename/{forename}")
    public @ResponseBody List<User> getUserByForename(@PathVariable String forename, Pageable pageable) {
        return userRepository.findByForename(forename, pageable);
    }

    @GetMapping(path="/surname/{surname}")
    public @ResponseBody List<User> getUserBySurname(@PathVariable String surname, Pageable pageable) {
        return userRepository.findBySurname(surname, pageable);
    }

    @GetMapping(path="/department/{department}")
    public @ResponseBody List<User> getUserByDepartment(@PathVariable String department, Pageable pageable) {
        return userRepository.findByDepartment(department, pageable);
    }

    @GetMapping(path="/username/{username}")
    public @ResponseBody User getUserByUsername(@PathVariable String username) {
        return userRepository.findByUsername(username).get();
    }

    @GetMapping(path="/phone/{phone}")
    public @ResponseBody User getUserByPhone(@PathVariable String phone) {
        return userRepository.findByPhone(phone);
    }

    @GetMapping(path="/email/{email}")
    public @ResponseBody User getUserByEmail(@PathVariable String email) {
        return userRepository.findByEmail(email);
    }

    public static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    public static String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<String>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }


}