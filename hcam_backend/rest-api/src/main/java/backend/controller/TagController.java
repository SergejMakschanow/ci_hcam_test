package backend.controller;

import backend.model.Tag;
import backend.model.projections.TagBaseProjection;
import backend.repositories.TagRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static backend.controller.UserController.getNullPropertyNames;

@Controller
@RequestMapping(TagController.BASE_URL)
public class TagController {

    public static final String BASE_URL = "/tags";

    @Autowired
    private TagRepository tagRepository;

    /*@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER'")")*/
    @GetMapping
    public ResponseEntity getAllTags() {
        Collection<TagBaseProjection> tags = tagRepository.findAllProjectedBy(TagBaseProjection.class);
        tags = tags.stream().distinct().collect(Collectors.toList());
        return ResponseEntity.ok(tags);
    }

    /*@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER'")")*/
    @GetMapping(path = "/inactive", produces = "application/json")
    public ResponseEntity getInactiveTags() {
        Collection<TagBaseProjection> inactiveTags = tagRepository.findByActive(false, TagBaseProjection.class);
        return ResponseEntity.ok(inactiveTags);
    }

    @GetMapping(path = "/category", produces = "application/json")
    public ResponseEntity getCategory() {
        class ResponseWrapper {
            public Collection<String> categories = new HashSet<>();

            public Collection<String> getCategories() {
                return categories;
            }

            public void setCategories(Collection<String> categories) {
                this.categories = categories;
            }
        }

        ResponseWrapper rw = new ResponseWrapper();
        tagRepository.findAll().forEach(tag -> rw.categories.add(tag.getCategory()));
        return ResponseEntity.ok(rw);
    }

    /*@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER'")")*/
    @DeleteMapping(path = "/{id}")
    public ResponseEntity deleteTag(@PathVariable int id) {
        if (!tagRepository.exists(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            System.out.println(id);
            tagRepository.delete(id);
            //System.out.println(tag);
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @PostMapping(path = "/create")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody ResponseEntity createTag(@RequestBody Tag tag) {
        tagRepository.save(tag);
        int latEntryId =  tagRepository.getLastInsertId();
        return ResponseEntity.ok(tagRepository.findById(latEntryId, TagBaseProjection.class));
    }

    /* @PreAuthorize("hasAnyRole('ADMIN')") */
    @PutMapping(path = "/{id}")
    public Tag updateTag(@RequestBody Tag tag, @PathVariable int id) {
        Tag existing = tagRepository.findById(id);
        copyNonNullProperties(tag, existing);
        return tagRepository.save(existing);
    }

    public static <T> Predicate<T> distinctByKey(
            Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }


}