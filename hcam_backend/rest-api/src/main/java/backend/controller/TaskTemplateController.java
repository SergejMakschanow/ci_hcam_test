package backend.controller;

import backend.model.TaskTemplate;
import backend.model.projections.CustomTaskTypes;
import backend.model.projections.TaskTemplateProjection;
import backend.repositories.TaskTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(TaskTemplateController.BASE_URL)
public class TaskTemplateController {

    public static final String BASE_URL = "/taskTemplates";

    @Autowired
    private TaskTemplateRepository taskTemplateRepository;

    @GetMapping
    public ResponseEntity getTaskTemplates() {
        final Collection<CustomTaskTypes> taskTemplates = this.taskTemplateRepository.findAllProjectedBy(CustomTaskTypes.class);
        return ResponseEntity.ok(taskTemplates);
    }

    @GetMapping(path = "/{idTaskTemplate}")
    public ResponseEntity getTaskTemplateById(@PathVariable int idTaskTemplate) {
        final TaskTemplateProjection taskTemplate = this.taskTemplateRepository.findById(TaskTemplateProjection.class, idTaskTemplate);
        return ResponseEntity.ok(taskTemplate);
    }

    @GetMapping(path = "/listByIds")
    public ResponseEntity getTaskTemplateById(@RequestBody Map<String, List<Integer>> taskTemplateByIdList) {
        List<TaskTemplateProjection> result = new ArrayList<>();
        taskTemplateByIdList.values().stream()
                                        .flatMap(Collection::stream)
                                        .collect(Collectors.toList())
                                        .forEach(id -> result.add (this.taskTemplateRepository.findById(TaskTemplateProjection.class, id)));

        return ResponseEntity.ok(result);
    }
}