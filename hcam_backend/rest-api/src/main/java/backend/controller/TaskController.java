package backend.controller;

import backend.model.Asset;
import backend.model.Task;
import backend.model.TaskTemplate;
import backend.model.User;
import backend.model.projections.CustomGetFilteredTasks;
import backend.model.projections.CustomTask;
import backend.model.wrapper.TaskPutWrapper;
import backend.model.wrapper.TaskRequestBody;
import backend.repositories.AssetRepository;
import backend.repositories.TaskRepository;
import backend.repositories.TaskTemplateRepository;
import backend.repositories.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

import static backend.controller.UserController.getNullPropertyNames;

@Controller
@RequestMapping(TaskController.BASE_URL)
public class TaskController {

    public static final String BASE_URL = "/tasks";

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskTemplateRepository taskTemplateRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AssetRepository assetRepository;

    @GetMapping
    public ResponseEntity findAllTasks() {
        final Collection<CustomTask> tasks = this.taskRepository.findAllProjectedBy(CustomTask.class);
        return ResponseEntity.ok(tasks);
    }

    @GetMapping(path="/current/{username}")
    public ResponseEntity filteredByUsername(@PathVariable String username) {
        final CustomGetFilteredTasks tasks = this.taskRepository.findTaskByUserUsername(username, CustomGetFilteredTasks.class);
        return ResponseEntity.ok(tasks);
    }

    @DeleteMapping(path="/{idTask}")
    public ResponseEntity deleteTask(@PathVariable int idTask) {
        taskRepository.delete(idTask);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createTask(@RequestBody TaskRequestBody task) {
        Task newEntry = new Task();
        newEntry.setTitle(task.title);
        newEntry.setDescription(task.description);
        newEntry.setStartDate(task.startDate);
        newEntry.setFinishDate(task.finishDate);

        Optional<TaskTemplate> template = taskTemplateRepository.findById(task.idTaskTemplate);
        newEntry.setTaskTemplate(template.get());

        Optional<Asset> asset = assetRepository.findById(task.idAsset);
        newEntry.setAsset(asset.get());

        Optional<User> user = userRepository.findByUsername(task.username);
        newEntry.setUser(user.get());
        taskRepository.save(newEntry);
        return ResponseEntity.ok().build();
    }


    @GetMapping(path="/title/{title}")
    public @ResponseBody Task getTaskByTitle(@PathVariable String title) {
        return taskRepository.findByTitle(title);
    }

    @GetMapping(path="/state/{state}")
    public @ResponseBody Task getTaskByState(@PathVariable String state) {
        return taskRepository.findByState(state);
    }

    @PutMapping(path="/{idTask}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity updateTask(@RequestBody TaskPutWrapper task, @PathVariable int idTask) {
        Task oldTask = taskRepository.findById(idTask);
        copyNonNullProperties(task, oldTask);
        System.out.println(task.toString());

        Optional<Asset> asset = assetRepository.findById(task.idAsset);
        if(asset.isPresent()) oldTask.setAsset(asset.get());

        Optional<User> user = userRepository.findByUsername(task.username);
        if(user.isPresent()) oldTask.setUser(user.get());

        if(task.title != null)      oldTask.setTitle(task.title);
        if(task.description != null)oldTask.setDescription(task.description);
        if(task.startDate != null)  oldTask.setStartDate(task.startDate);
        if(task.finishDate != null) oldTask.setFinishDate(task.finishDate);
        taskRepository.save(oldTask);
        return ResponseEntity.accepted().build();
    }

    private static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

}