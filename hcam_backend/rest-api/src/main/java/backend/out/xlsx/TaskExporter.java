package backend.out.xlsx;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import backend.model.Asset;
import backend.model.Tag;
import backend.model.Task;
import backend.model.User;
import backend.repositories.TaskRepository;

@Component
public class TaskExporter extends ExcelExporter<TaskExportConfig> {

	private static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy hh:mm");

	@Autowired
	private TaskRepository taskRepository;

	private List<String> headers = Arrays.asList("ID", "Titel", "Beschreibung", "Startdatum", "Fälligkeitsdatum", "Bearbeiter", "Status", "Asset");

	private CellStyle headerStyle;

	@Override
	protected void init() {
		super.init();
		headerStyle = workbook.createCellStyle();
	}

	@Override
	protected void create() {
		sheet("test");
		writeHeader();
		taskRepository.findAll().stream().filter(config.getFilters()).forEach(this::writeTask);
		autosize();
	}

	private void writeTask(Task task) {
		nextRow();
		cell().setCellValue(task.getId());
		cell().setCellValue(task.getTitle());
		cell().setCellValue(task.getTaskTemplate().getDescription());
		cell().setCellValue(dateFormatter.format(task.getStartDate()));
		cell().setCellValue(dateFormatter.format(task.getFinishDate()));
		cell().setCellValue(getFullname(task));
		cell().setCellValue(task.getState());
		writeAsset(task);
	}

	private void writeAsset(Task task) {
		Asset asset = task.getAsset();
		cell().setCellValue(asset.getName());
		Iterator<Tag> iterator = asset.getTags().iterator();
		StringBuilder builder = new StringBuilder();
		if (iterator.hasNext()) {
			builder.append(iterator.next().getCategory());
			while (iterator.hasNext()) {
				builder.append(", ").append(iterator.next().getCategory());
			}
		}
		cell().setCellValue(builder.toString());
	}

	private String getFullname(Task task) {
		User user = task.getUser();
		if (user == null) {
			return "";
		}
		return user.getForename() + " " + user.getSurname();
	}

	@Override
	protected void writeHeader() {
		nextRow();
		for (String string : headers) {
			cell().setCellValue(string);
		}
	}

}
