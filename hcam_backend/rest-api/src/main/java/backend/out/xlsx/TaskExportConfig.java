package backend.out.xlsx;

import java.util.function.Predicate;

import backend.model.Task;

public class TaskExportConfig {

	public boolean excludeNotAssigned;
	public boolean closedTasks;

	public Predicate<Task> getFilters() {
		return t -> excludeNotAssigned ? t.getUser() != null : true;
	}

}
