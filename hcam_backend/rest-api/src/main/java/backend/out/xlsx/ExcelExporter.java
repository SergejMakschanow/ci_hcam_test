package backend.out.xlsx;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public abstract class ExcelExporter<T> {

	protected Workbook workbook;

	protected T config;

	protected Sheet sheet;

	protected Row row;

	public ExcelExporter() {
	}

	protected Cell cell() {
		return cell(null);
	}

	protected Cell cell(CellStyle style) {
		return cell(row.getLastCellNum() == -1 ? 0 : row.getLastCellNum(), style);
	}

	protected Cell cell(int col) {
		return cell(col, null);
	}

	protected Cell cell(int col, CellStyle style) {
		Cell cell = row.createCell(col);
		if (style != null) {
			cell.setCellStyle(style);
		}
		return cell;
	}

	protected Sheet sheet(String name) {
		sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName(name));
		return sheet;
	}

	public void write(OutputStream stream, T config) throws IOException {
		this.config = config;
		workbook = new XSSFWorkbook();
		sheet = null;
		row = null;
		init();
		create();
		workbook.write(stream);
	}

	protected Row nextRow() {
		if (row == null) {
			row = sheet.createRow(0);
			return row;
		}
		return row(sheet.getLastRowNum() + 1);
	}

	protected Row row(int rownum) {
		row = sheet.createRow(rownum);
		return row;
	}

	protected void autosize() {
		for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	protected void init() {
	}

	protected abstract void create();

	protected abstract void writeHeader();

}
