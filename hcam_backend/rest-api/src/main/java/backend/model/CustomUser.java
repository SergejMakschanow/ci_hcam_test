package backend.model;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "CustomUser", types = {User.class})
public interface CustomUser {
    String getUsername();
}
