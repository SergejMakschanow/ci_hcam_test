package backend.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class TaskLogEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    private String sourceTaskState;

    private String targetTaskState;

    @ManyToOne
    private Task task;

    //@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "role")
    //private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

        public String getSourceTaskState() {
        return sourceTaskState;
    }

    public void setSourceTaskState(String sourceTaskState) {
        this.sourceTaskState = sourceTaskState;
    }

        public String getTargetTaskState() {
        return targetTaskState;
    }

    public void setTargetTaskState(String targetTaskState) {
        this.targetTaskState = targetTaskState;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
