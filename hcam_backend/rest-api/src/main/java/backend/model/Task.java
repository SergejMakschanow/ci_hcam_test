package backend.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private int id;

    @Basic
    private String title;

    @Basic
    private String description;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;

    private String state;

    @ManyToOne
    private TaskTemplate taskTemplate;

    @ManyToOne
    private Position position;

    @ManyToOne
    private User user;

    @ManyToOne
    private Asset asset;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "task")
    private List<MultimediaFile> multimediaFiles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "task")
    private List<Checkpoints> checkpoints = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "task")
    private List<TaskLogEntry> taskLogEntries = new ArrayList<>();

    public Task() {}

    public Task(Task task) {
        this.id = task.getId();
        this.title = task.getTitle();
        this.description = task.getDescription();
        this.startDate = task.getStartDate();
        this.finishDate = task.getFinishDate();
        this.state = task.getState();
        this.taskTemplate = task.getTaskTemplate();
        this.position = task.getPosition();
        this.user = task.getUser();
        this.asset = task.getAsset();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public List<MultimediaFile> getMultimediaFiles() {
        return multimediaFiles;
    }

    public void setMultimediaFiles(List<MultimediaFile> multimediaFiles) {
        this.multimediaFiles = multimediaFiles;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public TaskTemplate getTaskTemplate() {
        return taskTemplate;
    }

    public void setTaskTemplate(TaskTemplate taskTemplate) {
        this.taskTemplate = taskTemplate;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public void unasign() {
        this.user = null;
    }

    public List<Checkpoints> getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(List<Checkpoints> checkpoints) {
        this.checkpoints = checkpoints;
    }

    public List<TaskLogEntry> getTaskLogEntries() {
        return taskLogEntries;
    }

    public void setTaskLogEntries(List<TaskLogEntry> taskLogEntries) {
        this.taskLogEntries = taskLogEntries;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                ", state='" + state + '\'' +
                ", taskTemplate=" + taskTemplate +
                ", position=" + position +
                ", user=" + user +
                ", asset=" + asset +
                ", checkpoints=" + checkpoints +
                ", taskLogEntries=" + taskLogEntries +
                '}';
    }
}