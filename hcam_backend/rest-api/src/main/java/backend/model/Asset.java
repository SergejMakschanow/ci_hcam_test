package backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean inspected;

    @Column(nullable = false)
    private boolean operative;

    @ManyToOne(cascade = CascadeType.ALL)
    private Position position;

    @ManyToOne
    private AssetType assetType;

    @OneToMany(mappedBy = "asset")
    private Set<Tag> tags = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "asset")
    private List<Task> tasks = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int idAsset) {
        this.id = idAsset;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getInspected() {
        return inspected;
    }

    public void setInspected(boolean inspected) {
        this.inspected = inspected;
    }

    public boolean getOperative() {
        return operative;
    }

    public void setOperative(boolean operative) {
        this.operative = operative;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public AssetType getAssetType() {
        return assetType;
    }

    public void setAssetType(AssetType assetType) {
        this.assetType = assetType;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void addToTags(Tag tag, Asset asset) {
        //tags.add(tag);
        //tag.getAsset().add(asset);
        //this.tags.add(tag);
    }

    @Override
    public String toString() {
        return "Asset{" +
                "idAsset=" + id +
                ", name='" + name + '\'' +
                ", inspected=" + inspected +
                ", operative=" + operative +
                ", idPosition=" + position +
                ", idAssetType=" + assetType +
                ", idTag=" + tags +
                '}';
    }
}
