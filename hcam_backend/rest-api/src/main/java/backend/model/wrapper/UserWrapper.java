package backend.model.wrapper;

import java.util.List;

public class UserWrapper {
    public String username;
    public String surname;
    public String forename;
    public String email;
    public String phone;
    public String department;
    public int idRole;
    public String password;
    public List<Integer> tasks;

    @Override
    public String toString() {
        return "UserWrapper{" +
                "username='" + username + '\'' +
                ", surname='" + surname + '\'' +
                ", forename='" + forename + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", department='" + department + '\'' +
                ", idRole=" + idRole +
                ", password='" + password + '\'' +
                '}';
    }
}
