package backend.model.wrapper;

import java.sql.Timestamp;

public class TaskRequestBody {
    public String title;
    public String description;
    public int idTaskTemplate;
    public Timestamp startDate;
    public Timestamp finishDate;
    public int idAsset;
    public String username;
}
