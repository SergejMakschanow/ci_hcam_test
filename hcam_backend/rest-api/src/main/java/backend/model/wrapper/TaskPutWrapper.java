package backend.model.wrapper;

import java.sql.Timestamp;

public class TaskPutWrapper {

    public int id;
    public String title;
    public String description;
    public Timestamp startDate;
    public Timestamp finishDate;
    public int idAsset;
    public String username;

    @Override
    public String toString() {
        return "TaskPutWrapper{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                ", idAsset=" + idAsset +
                ", username='" + username + '\'' +
                '}';
    }
}
