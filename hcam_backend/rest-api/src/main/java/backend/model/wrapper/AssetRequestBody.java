package backend.model.wrapper;

import java.util.List;

public class AssetRequestBody {

    public String name;

    public String room;

    public String building;

    public String level;

    public boolean operative;

    public boolean inspected;

    public List<Integer> tags;

}
