package backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class AssetType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String name;

    private String description;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "assetType")
    private List<TaskTemplate> taskTemplates = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assetType")
    private List<Checkpoints> checkpoints = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assetType")
    private List<Asset> assets = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<TaskTemplate> getTaskTemplates() {
        return taskTemplates;
    }

    public void setTaskTemplates(List<TaskTemplate> taskTemplates) {
        this.taskTemplates = taskTemplates;
    }

    public List<Checkpoints> getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(List<Checkpoints> checkpoints) {
        this.checkpoints = checkpoints;
    }

}
