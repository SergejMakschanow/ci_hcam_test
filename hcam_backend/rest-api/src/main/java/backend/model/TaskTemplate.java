package backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TaskTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private boolean repeatable;

    private String taskType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taskTemplate")
    private List<Task> tasks = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taskTemplate")
    private List<MultimediaFile> multimediaFiles = new ArrayList<>();

    @ManyToOne
    private AssetType assetType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taskTemplate")
    private List<Checkpoints> checkpoints = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTaskType() {
        return taskType;
    }

    public boolean isRepeatable() {
        return repeatable;
    }

    public void setRepeatable(boolean repeatable) {
        this.repeatable = repeatable;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public AssetType getAssetType() {
        return assetType;
    }

    public void setAssetType(AssetType assetType) {
        this.assetType = assetType;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Checkpoints> getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(List<Checkpoints> checkpoints) {
        this.checkpoints = checkpoints;
    }

    public List<MultimediaFile> getMultimediaFiles() {
        return multimediaFiles;
    }

    public void setMultimediaFiles(List<MultimediaFile> multimediaFiles) {
        this.multimediaFiles = multimediaFiles;
    }
}