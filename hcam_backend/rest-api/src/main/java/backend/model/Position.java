package backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private int id;

    @Column(nullable = false)
    private String building;

    @Column(nullable = false)
    private String room;

    @Column(nullable = false)
    private String level;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "position")
    private List<Asset> assets = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "position")
    private List<Task> tasks = new ArrayList<>();

    public Position() {
    }

    public Position(String building, String room, String level) {
        this.building = building;
        this.room = room;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
