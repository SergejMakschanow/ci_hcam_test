package backend.model.projections;

import backend.model.User;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "departmentOnly", types = User.class)
public interface CustomDepartmentOnly {
    String getDepartment();
}
