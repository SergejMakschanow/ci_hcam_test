package backend.model.projections;

import backend.model.Asset;
import backend.model.TaskTemplate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customAssetShortList", types = Asset.class)
public interface CustomAssetShortList {
    @JsonIgnore
    @JsonUnwrapped
    AssetTypeGetter getTasks();
    interface AssetTypeGetter {
        @JsonUnwrapped
        Bla getTaskTemplate();
        interface Bla {

            int getId();
        }
    }
    int getId();
    String getName();
}
