package backend.model.projections;

import backend.model.TaskTemplate;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customTaskTemplate", types = TaskTemplate.class)
public interface CustomTaskTypes {
    int getId();
    String getDescription();
}
