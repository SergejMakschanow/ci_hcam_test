package backend.model.projections;

import backend.model.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Check;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;
import java.util.List;

@Projection(name = "getFilteredTasks", types = Task.class)
public interface CustomGetFilteredTasks {

    //Zum späteren filtern:
    @Value("#{target.user.username}")
    String getUsername();

    int getId();
    String getTitle();
    String getDescription();

    @Value("#{target.taskTemplate.taskType}")
    String getTaskType();

    Timestamp getStartDate();
    Timestamp getFinishDate();

    @Value("#{target.taskTemplate.id}")
    int getIdTaskTemplate();

   /*
    AMultimediaFiles getMultimediaFiles();
    interface AMultimediaFiles {
        String getToken();
        String getName();
        String getType();
        Long getSize();
    }


    AGetCheckpoints getCheckpoints();
    interface AGetCheckpoints {
        @Value("#{target.id}")
        int getId();
        String getType();
        String getName();
        String getDefaultValue();
        String getValue();
        String getDescription();
    }
*/
   @Value("#{target.checkpoints}")
   List<Checkpoints> getCheckpoints();

    AssetId getAsset();
    interface AssetId {
        String getName();
        boolean getInspected();
        boolean getOperative();

        GetIdAssetType getAssetType();
        interface GetIdAssetType{
          int getId();
        }

        GetPosition getPosition();
        interface GetPosition {
            String getRoom();
            String getLevel();
            String getBuilding();
        }

        GetTags getTags();
        interface GetTags {
            String getUuid();
            boolean getActive();
            String getCategory();
        }
        //AMultimediaFiles getMultimediafiles();
    }
}