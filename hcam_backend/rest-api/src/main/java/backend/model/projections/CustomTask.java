package backend.model.projections;

import backend.model.Task;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;

@Projection(name = "customTask", types = Task.class)
public interface CustomTask {
    int getId();

    String getTitle();

    @Value("#{target.taskTemplate.taskType}")
    String getType();

    String getState();

    Timestamp getStartDate();

    Timestamp getFinishDate();

    @JsonUnwrapped
    UserCustom getUser();

    interface UserCustom {
        @Value("#{target.forename} #{target.surname}")
        String getFullname();
    }
}
