package backend.model.projections;

import backend.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customUser", types = User.class)
public interface CustomUser {
    int getId();
    String getUsername();
    String getSurname();
    String getForename();
    String getEmail();
    String getPhone();
    String getDepartment();

    @JsonUnwrapped
    RoleName getRole();
    interface RoleName {
        @JsonProperty("role")
        String getName();
    }
}
