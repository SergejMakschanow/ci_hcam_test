package backend.model.projections;

import backend.model.Role;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customRole", types = {Role.class})
public interface RoleBaseProjection {
    int getId();
    String getName();
}
