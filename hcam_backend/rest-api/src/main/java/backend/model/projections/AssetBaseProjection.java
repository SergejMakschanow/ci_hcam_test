package backend.model.projections;

import backend.model.Asset;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customAsset", types = Asset.class)
public interface AssetBaseProjection {
    int getId();
    String getName();
    boolean getInspected();
    boolean getOperative();

    @JsonProperty("tag")
    TagInfo getTags();
    interface TagInfo {
        String getUuid();
        String getCategory();
        boolean getActive();
    }

}