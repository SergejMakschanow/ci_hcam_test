package backend.model.projections;

import backend.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "shortList", types = User.class)
public interface CustomUserShortList {
    String getUsername();

    @JsonProperty("fullname")
    @Value("#{target.forename} #{target.surname}")
    String getFullname();
}
