package backend.model.projections;

import backend.model.Tag;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "tagCategoryProjection", types = Tag.class)
public interface TagCategoryProjection {

    String getCategory();
}
