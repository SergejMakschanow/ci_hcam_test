package backend.model.projections;

import backend.model.MultimediaFile;
import backend.model.TaskTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name="TaskTemplateProjection", types = TaskTemplate.class)
public interface TaskTemplateProjection {

    int getId();
    String getDescription();
    String getTaskType();
    boolean getRepeatable();

    MediaInfo getMultimediaFiles();
    interface MediaInfo {
        String getToken();
        String getType();
        String getName();
        Long getSize();
    }
}