package backend.model.projections;

import backend.model.Tag;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.rest.core.config.Projection;

@JsonPropertyOrder({"id", "uuid", "category", "active", "asset"})
@Projection(name = "customTag", types = Tag.class)
public interface TagBaseProjection {

    int getId();

    String getUuid();

    String getCategory();

    boolean getActive();

    @JsonProperty("asset")
    TagCustomAsset getAsset();

    @JsonPropertyOrder({"id", "name"})
    interface TagCustomAsset {
        Integer getId();
        String getName();
    }
}
