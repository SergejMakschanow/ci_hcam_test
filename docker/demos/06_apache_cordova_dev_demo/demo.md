touch Dockerfile
docker build --tag test/apache_cordova .
mkdir App
cd App
docker run -it --volume "$(pwd)":/App --workdir /App test/apache_cordova
docker-compose --file docker-compose.yaml up -d
docker-compose --file docker-compose-dev.yaml up -d
docker-compose --project-name prod --file docker-compose.yaml up -d
docker-compose --project-name dev --file docker-compose-dev.yaml up -d
