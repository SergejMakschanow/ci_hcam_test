package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.WebApplicationType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collections;
import java.lang.Iterable;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.example.demo.User;
import com.example.demo.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class DemoApplication {


	@Autowired 
	private UserRepository userRepository;

    	@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    	public ResponseEntity<?> home() {
        	return ResponseEntity.ok(Collections.singletonMap("response","Hello World!"));
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    	public ResponseEntity<?> getUsers() {

		final Gson gson = new GsonBuilder().create();
		final JsonObject json = new JsonObject();
		final Iterable<User> users = this.userRepository.findAll();
		json.add("users", gson.toJsonTree(users));

        	return ResponseEntity.ok(json.toString());
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    	public ResponseEntity<?> createUser(@RequestBody User user) {
		/*
		* TODO save user in database with 'this.userRepository'		
		*/
        	return new ResponseEntity(HttpStatus.CREATED);
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
    	public ResponseEntity<?> deleteUser() {
		/*
		* TODO delete user in database with 'this.userRepository'		
		*/
        	return new ResponseEntity(HttpStatus.OK);
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    	public ResponseEntity<?> updateUser(@RequestBody User user) {
		/*
		* TODO update user in database with 'this.userRepository'		
		*/
        	return new ResponseEntity(HttpStatus.OK);
    	}
	
	public static void main(String[] args) {
        	new SpringApplicationBuilder(DemoApplication.class)
            	.web(WebApplicationType.SERVLET) // .NONE, .REACTIVE, .SERVLET
            	.run(args);
   	}	
}
