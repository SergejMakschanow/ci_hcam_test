touch app.py && touch Dockerfile && touch docker-compose.yml && touch requirements.txt
service docker start
//add 'volumes: - .:/code' to web service in docker-compose.yml. Now code changes are possible while the app is running.
