mvn archetype:generate -DgroupId=org.examples.java -DartifactId=helloworld -DinteractiveMode=false
cd helloworld/
mvn package
java -cp target/helloworld-1.0-SNAPSHOT.jar org.examples.java.App
touch Dockerfile
docker build -t hello-java:latest .
docker image ls
docker container run hello-java:latest
