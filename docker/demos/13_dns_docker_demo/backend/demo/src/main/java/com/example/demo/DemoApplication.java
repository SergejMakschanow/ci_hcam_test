package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.WebApplicationType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.example.demo.User;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class DemoApplication {

	final Map<Integer, User> users = new HashMap<Integer, User>()
	{
	    {
		put(0, new User("Its", "Me"));
		put(1, new User("Not", "You"));
	    }
	};

    	@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    	public ResponseEntity<?> home() {
        	return ResponseEntity.ok(Collections.singletonMap("response","Hello World!"));
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    	public ResponseEntity<?> getUsers() {

		final Gson gson = new GsonBuilder().create();
		final JsonObject json = new JsonObject();
		json.add("users", gson.toJsonTree(new ArrayList<User>(this.users.values())));

        	return ResponseEntity.ok(json.toString());
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    	public ResponseEntity<?> createUser(@RequestBody User user) {
		user.setID(this.users.size());
		this.users.put(user.getID(), user);
        	return new ResponseEntity(HttpStatus.CREATED);
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
    	public ResponseEntity<?> deleteUser() {
		if(this.users.size() > 0) {
			this.users.remove(this.users.size()-1);
		}
        	return new ResponseEntity(HttpStatus.OK);
    	}

    	@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    	public ResponseEntity<?> updateUser(@RequestBody User user) {
		if(this.users.size() > 0) {
			this.users.put(this.users.size()-1,user);
		}
        	return new ResponseEntity(HttpStatus.OK);
    	}
	
	public static void main(String[] args) {
        	new SpringApplicationBuilder(DemoApplication.class)
            	.web(WebApplicationType.SERVLET) // .NONE, .REACTIVE, .SERVLET
            	.run(args);
   	}	
}
