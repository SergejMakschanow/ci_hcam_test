# demo docker with dns

# author: schiko

# steps to prepare the system (Linux)

### option 1
systemctl start docker

### option 2
service docker start

# check weather docker is running

### option 1
systemctl status docker

### option 2
service docker status


# run demo with docker-compose

### first step: build docker images
docker-compose build

### second step: start all docker container
### localhost:5800 shows client view in browser (firefox)
docker-compose up -d

### last step: shutdown all docker container
docker-compose down
