window.onload = function() {
	document.getElementById("GET").addEventListener("click", function(){
		fetch("http://www.api.hcam.org")
			.then(response => response.json())
			.then(result => document.getElementById("result").innerText = "["+result.response+"]")
			.catch(error => console.error('Error:', error));
	});
}
