mvn package
docker-compose build
docker-compose up -d
docker container ls
mvn archetype:generate -DgroupId=org.hcam.test -DartifactId=test -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
