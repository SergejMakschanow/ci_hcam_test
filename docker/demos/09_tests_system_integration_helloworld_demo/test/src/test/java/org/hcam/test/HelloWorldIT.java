package org.hcam.test;

import com.consol.citrus.annotations.CitrusResource;
import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.dsl.junit.JUnit4CitrusTestRunner;
import com.consol.citrus.dsl.runner.TestRunner;
import org.junit.Test;
import org.junit.Ignore;

/**
 * HelloWorld integration test
 *
 * @author Schiko
 * @since 2019-04-17
 */
public class HelloWorldIT extends JUnit4CitrusTestRunner {
    @CitrusTest
    @Test
    public void helloWorld(@CitrusResource TestRunner testRunner) {
        testRunner.echo("TODO: Code the test HelloWorld");
    }
}
