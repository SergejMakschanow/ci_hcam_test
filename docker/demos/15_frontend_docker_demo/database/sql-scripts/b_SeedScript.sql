USE `swtpj`;

Insert Into Position
(building, room, `level`)
Values
('18', '0', '1'),
('42', '5', '69');

Insert Into AssetType
(name, description)
Values
('Modell E034', 'Bitte erst Geraet ausschalten'),
('Modell XF90', 'Hoehenverstellbar');

Insert Into Asset
(name, inspected, operative, Position_idPosition, AssetType_idAssetType)
Values
('Brandschutztuer', '0', '1', 0, 0),
('Krankenbett', '0', '1', 0, 0);


/*  
category -> description
*/  
Insert Into Tag
(uuid, category, description, active, Asset_idAsset)     
Values
('1234', 'Beacon', 'IBeacon', '1', 0),
('1235', 'QR', 'QR Code', '1', 0);


/*
nicht im Klassendiagramm
*/
Insert Into AssetFeatures
(featureName, AssetType_idAssetType)
Values
('Kopfteil abnehmbar', 0),
('Hoehenverstellbar', 0);

/*
taskType?
*/
Insert Into TaskTemplate
(type, repeats, taskType, AssetType_idAssetType)
Values
('Betten neu beziehen', 1, '', 0),
('Pumpenanlage pruefen', 2, '', 0);

Insert Into User
(forename, surname, department, password, personalID, phone, email, Role_idRole, Group_idGroup)
Values
('Hans', 'Maelzer','EDV-Leiter','Hans123','1','0163254138', 'hans.maelzer@uksh.de',0 ,0),
('Joachim', 'Bauer','System Admin','password','2','01735876169', 'joachim.bauer@uksh.de',0 ,0);

Insert Into Task
(title, startDate, finishDate, state, TaskTemplate_idTaskTemplate, Position_idPosition, User_idUser, Asset_idAsset)
Values
('Pruefautrag', '2019-04-23 05:54:23', '2019-06-01 05:05:23', 'in process', 0, 0, 0, 0),
('Pruefautrag', '2019-06-11 13:05:17', '2020-01-15 13:05:23', 'ready to start', 0, 0, 0, 0);


/*
value ist im Klassendiagramm ein String
*/
Insert Into Checkpoints
(name, value, description, type, defaultValue, AssetType_idAssetType, TaskTemplate_idTaskTemplate, Task_idTask)
Values
('Schanier geoelt', 'true', 'WD50 verwenden', 'boolean', 'false', 0, 0, 0),
('Kopfkissen neu bezogen', 'true', 'Allergiker beachten', 'boolean', 'false', 0, 0, 0);

/*
name = Name der Datei oder eine Beschreibung
*/
Insert Into MultimediaFile
(token, type, name, TaskTemplate_idTaskTemplate, Task_idTask)
Values
('http://www.google.de', 'jpeg', 'Schaltplan',0 ,0),
('http://www.google.de', 'pdf', 'Benutzerhandbuch Bettenmodell XF90',0 ,0);

Insert Into TaskHistory
(timestamp, sourceTaskState, targetTaskState, Task_idTask, User_idUser)
Values
('2019-01-01 05:05:23', 'in process', 'ready to start', 0, 0),
('2019-04-01 19:37:12', 'in process', 'ready to start', 0, 0);

Insert Into `Group` 
(name) 
Values 
('Admin'),
('Manager'),
('LPU');
