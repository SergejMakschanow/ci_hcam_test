-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema swtpj
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `swtpj` DEFAULT CHARACTER SET utf8 ;
USE `swtpj` ;

-- -----------------------------------------------------
-- Table `swtpj`.`Position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Position` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Position` (
  `idPosition` INT NOT NULL AUTO_INCREMENT,
  `building` VARCHAR(45) NOT NULL,
  `room` VARCHAR(45) NOT NULL,
  `level` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPosition`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`AssetType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`AssetType` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`AssetType` (
  `idAssetType` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`idAssetType`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`Asset`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Asset` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Asset` (
  `idAsset` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `inspected` TINYINT NOT NULL,
  `operative` TINYINT NOT NULL,
  `Position_idPosition` INT NOT NULL,
  `AssetType_idAssetType` INT NOT NULL,
  PRIMARY KEY (`idAsset`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`Tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Tag` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Tag` (
  `idTag` INT NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(45) NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `active` TINYINT NOT NULL,
  `Asset_idAsset` INT NOT NULL,
  PRIMARY KEY (`idTag`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`AssetFeatures`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`AssetFeatures` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`AssetFeatures` (
  `idAssetFeatures` INT NOT NULL AUTO_INCREMENT,
  `featureName` VARCHAR(45) NULL,
  `AssetType_idAssetType` INT NOT NULL,
  PRIMARY KEY (`idAssetFeatures`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`AssetType_has_AssetType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`AssetType_has_AssetType` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`AssetType_has_AssetType` (
  `AssetType_idAssetType` INT NOT NULL AUTO_INCREMENT,
  `AssetType_idAssetType1` INT NOT NULL,
  PRIMARY KEY (`AssetType_idAssetType`, `AssetType_idAssetType1`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`TaskTemplate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`TaskTemplate` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`TaskTemplate` (
  `idTaskTemplate` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  `repeats` INT NULL,
  `taskType` VARCHAR(45) NULL,
  `AssetType_idAssetType` INT NOT NULL,
  PRIMARY KEY (`idTaskTemplate`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`Group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Group` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Group` (
  `idGroup` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`idGroup`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`User` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`User` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `forename` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `department` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `personalID` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `Role_idRole` INT NOT NULL,
  `Group_idGroup` INT NOT NULL,
  PRIMARY KEY (`idUser`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`Task`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Task` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Task` (
  `idTask` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL,
  `startDate` DATETIME NULL,
  `finishDate` DATETIME NULL,
  `state` VARCHAR(45) NULL,
  `TaskTemplate_idTaskTemplate` INT NOT NULL,
  `Position_idPosition` INT NOT NULL,
  `User_idUser` INT NOT NULL,
  `Asset_idAsset` INT NOT NULL,
  PRIMARY KEY (`idTask`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`Checkpoints`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Checkpoints` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Checkpoints` (
  `idCheckpoints` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `value` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `type` VARCHAR(45) NULL,
  `defaultValue` VARCHAR(45) NULL,
  `AssetType_idAssetType` INT NOT NULL,
  `TaskTemplate_idTaskTemplate` INT NOT NULL,
  `Task_idTask` INT NOT NULL,
  PRIMARY KEY (`idCheckpoints`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`MultimediaFile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`MultimediaFile` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`MultimediaFile` (
  `idMultimediaFile` INT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(45) NULL,
  `type` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `size` BIGINT NULL,
  `TaskTemplate_idTaskTemplate` INT NOT NULL,
  `Task_idTask` INT NOT NULL,
  PRIMARY KEY (`idMultimediaFile`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`MultimediaFile_has_AssetType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`MultimediaFile_has_AssetType` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`MultimediaFile_has_AssetType` (
  `MultimediaFile_idMultimediaFile` INT NOT NULL AUTO_INCREMENT,
  `AssetType_idAssetType` INT NOT NULL,
  PRIMARY KEY (`MultimediaFile_idMultimediaFile`, `AssetType_idAssetType`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`MultimediaFile_has_Asset`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`MultimediaFile_has_Asset` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`MultimediaFile_has_Asset` (
  `MultimediaFile_idMultimediaFile` INT NOT NULL AUTO_INCREMENT,
  `Asset_idAsset` INT NOT NULL,
  PRIMARY KEY (`MultimediaFile_idMultimediaFile`, `Asset_idAsset`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`TaskHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`TaskHistory` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`TaskHistory` (
  `idTaskHistory` INT NOT NULL AUTO_INCREMENT,
  `timestamp` DATETIME NULL,
  `sourceTaskState` VARCHAR(45) NULL,
  `targetTaskState` VARCHAR(45) NULL,
  `Task_idTask` INT NOT NULL,
  `User_idUser` INT NOT NULL,
  PRIMARY KEY (`idTaskHistory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`Permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Permission` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Permission` (
  `idPermission` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idPermission`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swtpj`.`Group_has_Permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swtpj`.`Group_has_Permission` ;

CREATE TABLE IF NOT EXISTS `swtpj`.`Group_has_Permission` (
  `Group_idGroup` INT NOT NULL AUTO_INCREMENT,
  `Permission_idPermission` INT NOT NULL,
  PRIMARY KEY (`Group_idGroup`, `Permission_idPermission`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
