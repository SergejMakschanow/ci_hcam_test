package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table (name = "User")
public class User {
    	@Id
    	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer idUser;
	private String forename;
	private String surname;
	private String department;
	private String password;
        private String personalID;
        private String phone;
        private String email; 
	private Integer role_idRole; 
	private Integer group_idGroup;

	public User(){};

	public void setId(final Integer id) {
		this.idUser = id;
	}

	public Integer getId() {
		return this.idUser;
	}
}
