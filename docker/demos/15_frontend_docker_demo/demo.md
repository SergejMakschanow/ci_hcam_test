# Avoid starting tests and database connection test in backend. Otherwise error due to docker database 
# (see. application.properties)
mvn clean package -DskipTests
