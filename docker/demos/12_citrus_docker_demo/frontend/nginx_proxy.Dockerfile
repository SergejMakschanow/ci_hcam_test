FROM nginx:1.15.11
MAINTAINER schiko
COPY app /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
