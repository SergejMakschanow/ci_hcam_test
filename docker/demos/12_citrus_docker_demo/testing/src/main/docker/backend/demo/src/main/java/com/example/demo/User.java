package com.example.demo;

public class User {
	private Integer id;
	private final String firstName;
	private final String lastName;
	public User(final String fn, final String ln) {
		this.firstName = fn;
		this.lastName = ln;
	}

	public void setID(final Integer id) {
		this.id = id;
	}

	public Integer getID() {
		return this.id;
	}
}
