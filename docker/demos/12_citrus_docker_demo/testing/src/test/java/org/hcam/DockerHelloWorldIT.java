package org.hcam;

import org.testng.annotations.Test;

import com.consol.citrus.annotations.CitrusXmlTest;
import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.dsl.testng.TestNGCitrusTestDesigner;
import org.springframework.http.HttpStatus;

/**
 * org.hcam
 *
 * @author Schiko
 * @since 2019-04-20
 */
@Test
public class DockerHelloWorldIT extends TestNGCitrusTestDesigner{

    //TODO find package of CommandResultCallback<Version>
    public void dockerStartContainerWithCallBack() {
    //	docker().version()
    //    	.validateCommandResult(new CommandResultCallback<Version>() {
    //        		@Override
    //        		public void doWithCommandResult(Version version, TestContext context) {
    //            		Assert.assertEquals(version.getApiVersion(), "1.20");
    //        		}
    //	});

    //	docker().ping();

    //	docker().start("my_container");
   }

    @CitrusTest
    public void dockerPing() {

    	docker().ping();
   }

    @CitrusTest
    public void dockerStartContainer() {
	final String helloWorld = "hcam/backend";
    	docker().create(helloWorld);
	//docker().start(helloWorld);
   }

   @CitrusTest
   public void httpActionTest() {
    http().client("helloHttpClient")
	  .send()
	  .get()
	  .contentType("application/json")
	  .accept("application/json");

    http().client("helloHttpClient")
	  .receive()
	  .response(HttpStatus.OK)
          .payload("{\"response\":\"Hello World!\"}") //escape double quotes to match the payload
	  .version("HTTP/1.1");
   }
}
