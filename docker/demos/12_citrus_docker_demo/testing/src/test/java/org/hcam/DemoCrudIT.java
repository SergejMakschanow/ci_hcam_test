package org.hcam;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import com.consol.citrus.annotations.CitrusXmlTest;
import com.consol.citrus.dsl.testng.TestNGCitrusTestDesigner;
import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.http.client.HttpClient;
import org.springframework.http.HttpStatus;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

/**
 * org.hcam
 *
 * @author Schiko
 * @since 2019-04-21
 */
@Test
public class DemoCrudIT extends TestNGCitrusTestDesigner {

    @CitrusTest
    public void createUser(){
	
	http().client("helloHttpClient")
	  .send()
	  .post("users")
          .payload("{\"firstName\":\"We\",\"lastName\":\"are\"}")
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("helloHttpClient")
	  .receive()
	  .response(HttpStatus.CREATED)
	  .version("HTTP/1.1");

    }

    @CitrusTest
    public void createUserWithoutPayload(){
	
    	http().client("helloHttpClient")
	  .send()
	  .post("users")
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("helloHttpClient")
	  .receive()
	  .response(HttpStatus.BAD_REQUEST)
	  .version("HTTP/1.1");

    }

    @CitrusTest
    public void readUsers(){
	
    	http().client("helloHttpClient")
	  .send()
	  .get("users")
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("helloHttpClient")
	  .receive()
	  .response(HttpStatus.OK)
	  .version("HTTP/1.1")
	  .validate("$.users.length()", greaterThanOrEqualTo(0));

    }

    @CitrusTest
    public void updateUser(){
	
    	http().client("helloHttpClient")
	  .send()
	  .put("users")
          .payload("{\"firstName\":\"I'am\",\"lastName\":\"a Update\"}")
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("helloHttpClient")
	  .receive()
	  .response(HttpStatus.OK)
	  .version("HTTP/1.1");

    }

    @CitrusTest
    public void deleteUser(){
	
    	http().client("helloHttpClient")
	  .send()
	  .delete("users")
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("helloHttpClient")
	  .receive()
	  .response(HttpStatus.OK)
	  .version("HTTP/1.1");

    }

}
