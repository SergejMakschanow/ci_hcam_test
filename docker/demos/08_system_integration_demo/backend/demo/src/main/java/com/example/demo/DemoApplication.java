package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.WebApplicationType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.http.MediaType;
import java.util.Collections;
import java.util.Map;

@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class DemoApplication {

    	@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
        @CrossOrigin(origins = "http://localhost")
    	Map hello() {
        	return Collections.singletonMap("response","Hello World! Docker rocks and is awesome!");
    	}
	
	public static void main(String[] args) {
        	new SpringApplicationBuilder(DemoApplication.class)
            	.web(WebApplicationType.SERVLET) // .NONE, .REACTIVE, .SERVLET
            	.run(args);
   	}	
}
