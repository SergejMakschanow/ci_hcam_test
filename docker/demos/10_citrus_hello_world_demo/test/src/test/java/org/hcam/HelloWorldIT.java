package org.hcam;

import org.testng.annotations.Test;

import com.consol.citrus.annotations.CitrusXmlTest;
import com.consol.citrus.testng.AbstractTestNGCitrusTest;

/**
 * org.hcam
 *
 * @author Schiko
 * @since 2019-04-19
 */
@Test
public class HelloWorldIT extends AbstractTestNGCitrusTest {

    @CitrusXmlTest(name = "HelloWorldIT")
    public void helloWorldIT() {}
}
