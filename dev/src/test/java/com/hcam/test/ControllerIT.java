package com.hcam.test;

import com.consol.citrus.dsl.testng.TestNGCitrusTestDesigner;
import org.springframework.http.HttpStatus;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Base class for the integration tests for the CRUD properties of all Controller of the hcam_backend
 *
 * @author Schiko
 * @since 2019-05-22
 */

public abstract class ControllerIT extends TestNGCitrusTestDesigner {

    protected void post(final String endpoint, final String payload, final HttpStatus expectedResponse){
	
	http().client("httpClient")
	  .send()
	  .post(endpoint)
          .payload(payload)
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("httpClient")
	  .receive()
	  .response(expectedResponse)
	  .version("HTTP/1.1");

    }

    protected void get(final String endpoint, final String endpointParameter, final HttpStatus expectedResponse, final Integer expectedElements){
	
    	http().client("httpClient")
	  .send()
	  .get(endpoint + endpointParameter)
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("httpClient")
	  .receive()
	  .response(expectedResponse)
	  .version("HTTP/1.1")
	  .validate("$.length()", greaterThanOrEqualTo(expectedElements));

    }

    protected void put(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	
	http().client("httpClient")
	  .send()
	  .post(endpoint)
          .payload(payload)
	  .contentType("application/json")
	  .accept("application/json");
	
    	http().client("httpClient")
	  .send()
	  .put(endpoint + endpointParameter)
          .payload(payload)
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("httpClient")
	  .receive()
	  .response(expectedResponse)
	  .version("HTTP/1.1");

    }

    protected void delete(final String endpoint, final String endpointParameter, final String payload, final HttpStatus expectedResponse){
	
	http().client("httpClient")
	  .send()
	  .post(endpoint)
          .payload(payload)
	  .contentType("application/json")
	  .accept("application/json");
	
    	http().client("httpClient")
	  .send()
	  .delete(endpoint + endpointParameter)
	  .contentType("application/json")
	  .accept("application/json");

    	http().client("httpClient")
	  .receive()
	  .response(expectedResponse)
	  .version("HTTP/1.1");

    }

    protected Object[][] mergeTestData(final Object[][] positiveTestData, final Object[][] negativeTestData) {
    	final List<Object[]> result = new ArrayList<>();
  	result.addAll(Arrays.asList(positiveTestData));
	result.addAll(Arrays.asList(negativeTestData));
  	return result.toArray(new Object[result.size()][]);
    }	

}
